﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CustomerApp.Models;
using System.Web.Http.Description;

namespace CustomerApp.Controllers
{
    [RoutePrefix("api/Person")]
    public class PersonController : ApiController
    {
        static readonly IPersonRepository repository = new PersonRepository();

        public IEnumerable<Person> GetAllPersons()
        {
            return repository.GetAll();
        }

        [Route("{id}")]
        public IEnumerable<Person> GetPerson(string id)
        {
            List<Person> retVal = new List<Person>();

            int result;

            if (int.TryParse(id, out result))
            {
                Person person = repository.Get(id);

                retVal.Add(person);
            }
            else
            {
                IEnumerable<Person> list = GetPerson(id, "");

                retVal = list.ToList<Person>();

                if (retVal.Count == 0)
                {
                    throw new HttpResponseException(HttpStatusCode.NotFound);
                }
            }

            return retVal;
        }

        [Route("{fname}/{lname}")]
        public IEnumerable<Person> GetPerson(string fname, string lname)
        {
            IEnumerable<Person> list = GetAllPersons();

            List<Person> tempList = list.ToList<Person>();

            List<Person> retVal = new List<Person>();

            foreach (Person curPerson in tempList)
            {
                if (lname == "")
                {
                    if (curPerson.FirstName.Contains(fname) ||
                        curPerson.LastName.Contains(fname))
                    {
                        retVal.Add(curPerson);
                    }
                }
                // Checks if the current person on the lish
                // contains any of the search parameters
                else if (curPerson.FirstName.Contains(fname) || 
                    curPerson.LastName.Contains(lname) || 
                    curPerson.FirstName.Contains(lname) || 
                    curPerson.LastName.Contains(fname))
                {
                    retVal.Add(curPerson);
                }
            }

            return retVal;
        }

        [Route("{person}")]
        public HttpResponseMessage Post(Person person)
        {
            person = repository.Add(person);
            var response = Request.CreateResponse<Person>(HttpStatusCode.Created, person);

            string uri = Url.Link("ControllerOnly", new { personID = person.PersonID });
            response.Headers.Location = new Uri(uri);
            return response;
        }

        [Route("{person}")]
        public HttpResponseMessage Put(Person person)
        {
            var response = Request.CreateResponse<Person>(
                    HttpStatusCode.NotModified, person);

            IEnumerable<Person> personToFind = GetPerson(person.PersonID.ToString());

            if (personToFind.Count() > 0)
            {
                bool result = repository.Update(person);

                if (result)
                {
                    response = Request.CreateResponse<Person>(
                            HttpStatusCode.OK, person);
                }
            }

            return response;
        }

        [Route("{id}")]
        public HttpResponseMessage Delete(string id)
        {
            var response = Request.CreateResponse(
                HttpStatusCode.NotModified);

            Person person = repository.Get(id);

            if (person != null)
            {
                bool isRemoved = repository.Remove(id);

                if (isRemoved)
                {
                    response = Request.CreateResponse(
                        HttpStatusCode.OK);
                }
            }

            return response;
        }
    }
}