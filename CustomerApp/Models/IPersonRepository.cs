﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomerApp.Models
{
    public interface IPersonRepository
    {
        IEnumerable<Person> GetAll();
        Person Get(string personID);
        Person Add(Person item);
        bool Remove(string personID);
        bool Update(Person item);
    }
}
