﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CustomerApp.Models
{
    public class PersonRepository : IPersonRepository
    {
        public IEnumerable<Person> GetAll()
        {
            List<Person> persons = new List<Person>();
            string query = string.Format("SELECT * FROM phonebookschema.recordstbl");

            using (MySqlConnection con =
                    new MySqlConnection("Server=localhost;Database=phonebookschema;Uid=root;Pwd=root;"))
            {
                using (MySqlCommand cmd = new MySqlCommand(query, con))
                {
                    con.Open();
                    MySqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        Person person = new Person();
                        person.PersonID = reader.GetString(0);
                        person.FirstName = reader.GetString(1);
                        person.LastName = reader.GetString(2);
                        person.PhoneNumber = reader.GetString(3);
                        person.Address = reader.GetString(4);

                        persons.Add(person);
                    }
                    con.Close(); 
                }
            }
            return persons;
        }

        public Person Get(string personID)
        {
            Person person = new Person();
            string query = string.Format(" SELECT * FROM phonebookschema.recordstbl" +
                "  WHERE id = "+ personID);

            using (MySqlConnection con =
                    new MySqlConnection("Server=localhost;Database=phonebookschema;Uid=root;Pwd=root;"))
            {
                using (MySqlCommand cmd = new MySqlCommand(query, con))
                {
                    con.Open();
                    MySqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        person.PersonID = reader.GetString(0);
                        person.FirstName = reader.GetString(1);
                        person.LastName = reader.GetString(2);
                        person.PhoneNumber = reader.GetString(3);
                        person.Address = reader.GetString(4);

                    }
                    con.Close();
                }
            }
            return person;
        }

        public Person Add(Person person)
        {            
            string query = string.Format("INSERT INTO phonebookschema.recordstbl " + 
                "(f_name, l_name, phone_number, address) " +
                       "VALUES ('{0}', '{1}', '{2}', '{3}')", 
                       person.FirstName, person.LastName, person.PhoneNumber, person.Address);

            using (MySqlConnection con =
                    new MySqlConnection("Server=localhost;Database=phonebookschema;Uid=root;Pwd=root;"))
            {
                using (MySqlCommand cmd = new MySqlCommand(query, con))
                {
                    con.Open();
                    cmd.ExecuteNonQuery();

                    query = "SELECT LAST_INSERT_ID()";
                    MySqlCommand cmd2 = new MySqlCommand(query, con);
                    MySqlDataReader reader = cmd2.ExecuteReader();
                    if (reader.Read())
                    {
                        person.PersonID = reader.GetString(0);
                    }

                    con.Close();
                }
            }

            return person;
        }

        public bool Remove(string personID)
        {
            string query = string.Format("DELETE FROM phonebookschema.recordstbl WHERE id = {0}", personID); 

            using (MySqlConnection con =
                    new MySqlConnection("Server=localhost;Database=phonebookschema;Uid=root;Pwd=root;"))
            {
                using (MySqlCommand cmd = new MySqlCommand(query, con))
                {
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }

            return true;
        }

        public bool Update(Person person)
        {
            string query = string.Format("UPDATE phonebookschema.recordstbl " +
                    "SET f_name = '{0}', " +
                    "l_name = '{1}', " +
                    "phone_number = '{2}', " +
                    "address = '{3}' " +
                    "WHERE id = {4}", 
                    person.FirstName, 
                    person.LastName, 
                    person.PhoneNumber, 
                    person.Address, 
                    person.PersonID);

            using (MySqlConnection con =
                    new MySqlConnection("Server=localhost;Database=phonebookschema;Uid=root;Pwd=root;"))
            {
                using (MySqlCommand cmd = new MySqlCommand(query, con))
                {
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }

            return true;
        }
    }
}